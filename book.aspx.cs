﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string train_id = Request.QueryString["train_id"];
            string start = Request.QueryString["start"];
            string end = Request.QueryString["end"];
            string date = Request.QueryString["date"];
            txt_trainID.Text = train_id;
            DropDownList1.SelectedIndex = Convert.ToInt32(start) - 1;
            DropDownList2.SelectedIndex = Convert.ToInt32(end) - 1;
            if (Request.QueryString["date"] == null)
                lbl_date.Text = DateTime.Today.ToString("yyyy/MM/dd");
            else
                lbl_date.Text = date;
        }
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (pl1.Visible)
        {
            pl1.Visible = false;
        }
        else
        {
            pl1.Visible = true;
        }
    }

    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        lbl_date.Text = Calendar1.SelectedDate.ToString("yyyy/MM/dd");
    }
    public int calc_null_seat()
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["trainConnectionString"].ConnectionString);
        conn.Open();
        SqlCommand cmd = new SqlCommand();
        if (Convert.ToInt32(DropDownList1.SelectedValue) < Convert.ToInt32(DropDownList2.SelectedValue))
            cmd.CommandText = "select seat_id from Seat where train_id=" + txt_trainID.Text + " and (dest_stn>" + DropDownList1.SelectedValue + " or origin_stn<" + DropDownList2.SelectedValue + ") order by seat_id";
        else
            cmd.CommandText = "select seat_id from Seat where train_id=" + txt_trainID.Text + " and (dest_stn<" + DropDownList1.SelectedValue + " or origin_stn>" + DropDownList2.SelectedValue + ") order by seat_id";
        cmd.Connection = conn;
        SqlDataReader read = cmd.ExecuteReader();
        int seatid = 1;
        while (read.Read())
        {
            if (seatid != Convert.ToInt32(read[0]))
            {
                break;
            }
            seatid += 1;
        }
        read.Close();
        cmd.CommandText = "select max_people from Train where train_id=" + txt_trainID.Text;
        read = cmd.ExecuteReader();
        read.Read();
        int max = Convert.ToInt32(read[0]);
        cmd.Cancel();
        read.Close();
        conn.Close();
        if (seatid <= max)
            return seatid;
        else
            return 0;

    }
    public int get_identity()
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["trainConnectionString"].ConnectionString);
        conn.Open();
        SqlCommand cmd = new SqlCommand();
        cmd.CommandText =
            "select book_id from Seat order by book_id desc";
        cmd.Connection = conn;
        SqlDataReader read = cmd.ExecuteReader();
        read.Read();
        int last = Convert.ToInt32(read[0]) + 1;
        return last;
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        int null_seat = calc_null_seat();
        int last_identity = get_identity();
        if (null_seat == 0)
            return;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["trainConnectionString"].ConnectionString);
        conn.Open();
        SqlCommand cmd = new SqlCommand("book", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@cus_id", SqlDbType.Char).Value = txt_cus_id.Text;
        cmd.Parameters.Add("@cus_name", SqlDbType.NVarChar).Value = txt_cus_name.Text;
        cmd.Parameters.Add("@train_id", SqlDbType.BigInt).Value = Convert.ToInt64(txt_trainID.Text);
        cmd.Parameters.Add("@start", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
        cmd.Parameters.Add("@end", SqlDbType.Int).Value = Convert.ToInt32(DropDownList2.SelectedValue);
        cmd.Parameters.Add("@seat_id", SqlDbType.Int).Value = null_seat;
        //cmd.Parameters.Add("@book_id", SqlDbType.Int).Value = last_identity;

        cmd.ExecuteNonQuery();

        //Response.Write("error");

        cmd.Cancel();
        cmd.CommandText = "success_show";
        cmd.Parameters.Clear();
        cmd.Parameters.Add("@cus_id", SqlDbType.Char).Value = txt_cus_id.Text;
        SqlDataReader read = cmd.ExecuteReader();
        read.Read();
        //string show = "訂票成功!";
        string show = "訂票成功!以下是訂票資訊:\\n";

        show += "訂票編號:" + read[0] + "\\n";
        show += "姓名:" + read[1] + "\\n";
        show += "火車編號:" + read[2] + "\\n";
        show += "座位編號:" + read[3] + "\\n";
        show += "起站點:" + read[4] + "\\n";
        show += "到站點:" + read[5] + "\\n";
        ClientScript.RegisterStartupScript(GetType(), "message", "<script>alert('" + show + "');</script>");
        conn.Close();
    }
}