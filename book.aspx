﻿<%@ Page Title="訂票" Language="C#" MasterPageFile="~/customerMasterPage.master" AutoEventWireup="true" CodeFile="book.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row" style="height: 30px;"></div>
    <div class="row text-center">
        <table style="text-align: left;">
            <tr>
                <td>姓名<asp:TextBox ID="txt_cus_name" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="check" ControlToValidate="txt_cus_name" runat="server" ForeColor="Red" ErrorMessage="(必填)"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>身分證字號<asp:TextBox ID="txt_cus_id" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="check" runat="server" ControlToValidate="txt_cus_id" ForeColor="Red" ErrorMessage="(必填)"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationGroup="check" ControlToValidate="txt_cus_id" ValidationExpression="[A-Z][1-2]\d{8}" ForeColor="Red" runat="server" ErrorMessage="格式錯誤"></asp:RegularExpressionValidator>
                </td>
            </tr>
            <tr>
                <td>起站點<asp:DropDownList ID="DropDownList1" runat="server">
                    <asp:ListItem Value="1">南港</asp:ListItem>
                    <asp:ListItem Value="2">台北</asp:ListItem>
                    <asp:ListItem Value="3">板橋</asp:ListItem>
                    <asp:ListItem Value="4">桃園</asp:ListItem>
                    <asp:ListItem Value="5">新竹</asp:ListItem>
                    <asp:ListItem Value="6">苗栗</asp:ListItem>
                    <asp:ListItem Value="7">台中</asp:ListItem>
                    <asp:ListItem Value="8">彰化</asp:ListItem>
                    <asp:ListItem Value="9">雲林</asp:ListItem>
                    <asp:ListItem Value="10">台南</asp:ListItem>
                    <asp:ListItem Value="11">左營</asp:ListItem>
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td>到站點<asp:DropDownList ID="DropDownList2" runat="server">
                    <asp:ListItem Value="1">南港</asp:ListItem>
                    <asp:ListItem Value="2">台北</asp:ListItem>
                    <asp:ListItem Value="3">板橋</asp:ListItem>
                    <asp:ListItem Value="4">桃園</asp:ListItem>
                    <asp:ListItem Value="5">新竹</asp:ListItem>
                    <asp:ListItem Value="6">苗栗</asp:ListItem>
                    <asp:ListItem Value="7">台中</asp:ListItem>
                    <asp:ListItem Value="8">彰化</asp:ListItem>
                    <asp:ListItem Value="9">雲林</asp:ListItem>
                    <asp:ListItem Value="10">台南</asp:ListItem>
                    <asp:ListItem Value="11">左營</asp:ListItem>
                </asp:DropDownList></td>
            </tr>
            <tr>
                <td>車次編號<asp:TextBox ID="txt_trainID" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="check" ControlToValidate="txt_trainID" runat="server" ForeColor="Red" ErrorMessage="(必填)"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>乘車日期<asp:Label ID="lbl_date" runat="server" Text="Label" BackColor="White"></asp:Label>
                    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/src/more.png" OnClick="ImageButton1_Click" /></td>
            </tr>

        </table>
        <div class="row">
            <asp:Panel ID="pl1" runat="server" Visible="false">
                <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged"></asp:Calendar>
            </asp:Panel>
        </div>
        <asp:Button ID="Button1" ValidationGroup="check" runat="server" Text="確定" OnClick="Button1_Click" />
    </div>
</asp:Content>

