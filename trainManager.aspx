﻿<%@ Page Title="火車管理" Language="C#" MasterPageFile="~/adminMasterPage.master" AutoEventWireup="true" CodeFile="trainManager.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        table {
            margin-left: auto;
            margin-right: auto;
        }

        .detail_tr {
            width: 50px;
        }

        .tb-right {
            text-align: left;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-xs-12">
        <div class="row" style="height: 50px;"></div>
        <div class="row text-center">
            <input id="btn_new_train" type="button" value="新增班次" />
            <div class="row" id="new_train" style="display: none;">
                <table class="tb-right">
                    <tr>
                        <td>起站點</td>
                        <td>
                            <asp:DropDownList ID="ddl_start" runat="server">
                                <asp:ListItem Value="1">南港</asp:ListItem>
                                <asp:ListItem Value="2">台北</asp:ListItem>
                                <asp:ListItem Value="3">板橋</asp:ListItem>
                                <asp:ListItem Value="4">桃園</asp:ListItem>
                                <asp:ListItem Value="5">新竹</asp:ListItem>
                                <asp:ListItem Value="6">苗栗</asp:ListItem>
                                <asp:ListItem Value="7">台中</asp:ListItem>
                                <asp:ListItem Value="8">彰化</asp:ListItem>
                                <asp:ListItem Value="9">雲林</asp:ListItem>
                                <asp:ListItem Value="10">台南</asp:ListItem>
                                <asp:ListItem Value="11">左營</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>到站點</td>
                        <td>
                            <asp:DropDownList ID="ddl_end" runat="server">
                                <asp:ListItem Value="1">南港</asp:ListItem>
                                <asp:ListItem Value="2">台北</asp:ListItem>
                                <asp:ListItem Value="3">板橋</asp:ListItem>
                                <asp:ListItem Value="4">桃園</asp:ListItem>
                                <asp:ListItem Value="5">新竹</asp:ListItem>
                                <asp:ListItem Value="6">苗栗</asp:ListItem>
                                <asp:ListItem Value="7">台中</asp:ListItem>
                                <asp:ListItem Value="8">彰化</asp:ListItem>
                                <asp:ListItem Value="9">雲林</asp:ListItem>
                                <asp:ListItem Value="10">台南</asp:ListItem>
                                <asp:ListItem Value="11">左營</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>發車時間</td>
                        <td>
                            <asp:DropDownList ID="ddl_y" runat="server">
                            </asp:DropDownList>年
                            <asp:DropDownList ID="ddl_m" runat="server">
                            </asp:DropDownList>月
                            <asp:DropDownList ID="ddl_d" runat="server">
                            </asp:DropDownList>日
                            <br />
                            <asp:DropDownList ID="ddl_hh" runat="server">
                            </asp:DropDownList>時
                            <asp:DropDownList ID="ddl_mm" runat="server">
                            </asp:DropDownList>分
                            <asp:DropDownList ID="ddl_ss" runat="server">
                            </asp:DropDownList>秒
                        </td>
                    </tr>
                    <tr>
                        <td>最大人數</td>
                        <td>
                            <asp:DropDownList ID="ddl_max_people" runat="server">
                                <asp:ListItem Value="100">100人</asp:ListItem>
                                <asp:ListItem Value="200">200人</asp:ListItem>
                                <asp:ListItem Value="300">300人</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center">
                            <asp:Button ID="btn_new" runat="server" Text="新增" OnClick="btn_new_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp</td>
                    </tr>
                </table>
            </div>
        </div>
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <div class="row text-center">
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:trainConnectionString %>"
                        DeleteCommand="del_train"
                        DeleteCommandType="StoredProcedure">
                        <DeleteParameters>
                            <asp:Parameter Name="train_id" Type="Int64" />
                        </DeleteParameters>
                    </asp:SqlDataSource>
                    <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1" OnItemCommand="ListView1_ItemCommand">
                        <LayoutTemplate>
                            <table border="1" style="width: 80%;">
                                <tr>
                                    <td>火車編號</td>
                                    <td>起點站</td>
                                    <td>終點站</td>
                                    <td>出發時間</td>
                                    <td></td>
                                </tr>
                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                            </table>
                        </LayoutTemplate>
                        <ItemTemplate>
                            <tr>
                                <td><%#Eval("train_id")%></td>
                                <td><%#Eval("start_stn")%></td>
                                <td><%#Eval("end_stn")%></td>
                                <td><%#Eval("date_time")%></td>
                                <td>
                                    <asp:Button ID="btn_detail" runat="server" Text="詳細資料" CommandName="detail" />
                                    <asp:Button ID="Button1" runat="server" Text="刪除班次" CommandName="del" CommandArgument='<%#Eval("train_id")%>' OnClientClick="return confirm('確定刪除此班次?');" />
                                    <asp:HiddenField ID="hdf_Train_id" runat="server" Value='<%#Eval("train_id")%>' />
                                </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td colspan="4">
                                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:trainConnectionString %>"
                                        SelectCommand="train_detail"
                                        SelectCommandType="StoredProcedure">
                                        <SelectParameters>
                                            <asp:ControlParameter ControlID="hdf_Train_id" Name="train_id" Type="Int64" />
                                        </SelectParameters>
                                    </asp:SqlDataSource>
                                    <asp:ListView ID="ListView2" runat="server" DataSourceID="">
                                        <LayoutTemplate>
                                            <table>
                                                <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                                            </table>
                                        </LayoutTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td class="detail_tr"><%#Eval("cus_name")%></td>
                                                <td class="detail_tr"><%#Eval("seat_id")%></td>
                                                <td class="detail_tr"><%#Eval("o_stn")%></td>
                                                <td class="detail_tr"><%#Eval("d_stn")%></td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:ListView>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:ListView>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>
    <script>
        $(document).ready(function () {
            var button = document.getElementById('btn_new_train');
            button.onclick = function () {
                var div = document.getElementById('new_train');
                if (div.style.display !== 'none') {
                    div.style.display = 'none';
                }
                else {
                    div.style.display = 'block';
                }
            }
        });
    </script>
</asp:Content>

