﻿<%@ Page Title="查詢訂票結果" Language="C#" MasterPageFile="~/customerMasterPage.master" AutoEventWireup="true" CodeFile="book_result_search.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div class="row" style="height: 30px;"></div>
        <div class="text-center row">
            <table style="text-align: left;">
                <tr>
                    <td>身分證字號<asp:TextBox ID="txt_cus_id" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="check" ControlToValidate="txt_cus_id" ForeColor="Red" runat="server" ErrorMessage="(必填)"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="[A-Z][1-2]\d{8}" ValidationGroup="check" ControlToValidate="txt_cus_id" ForeColor="Red" runat="server" ErrorMessage="(格式錯誤)"></asp:RegularExpressionValidator>
                        <asp:Button ID="Button1" ValidationGroup="check" runat="server"  Text="查詢" />
                    </td>
                </tr>
            </table>
        </div>
        <div class="row" style="height:50px;"></div>
        <div class="row text-center">
            <asp:SqlDataSource ID="SqlDataSource1" runat="server"
                ConnectionString="<%$ ConnectionStrings:trainConnectionString %>"
                SelectCommand="search_book" SelectCommandType="StoredProcedure"
                DeleteCommand="del_book" DeleteCommandType="StoredProcedure"
                OnSelected="SqlDataSource1_Selected">
                <SelectParameters>
                    <asp:ControlParameter Name="cus_id" ControlID="txt_cus_id" Type="String" />
                </SelectParameters>
                <DeleteParameters>
                    <asp:Parameter Name="book_id" Type="Int64" />
                </DeleteParameters>
            </asp:SqlDataSource>
            <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand">
                <Columns>
                    <asp:BoundField DataField="bookid" HeaderText="訂票編號" />
                    <asp:BoundField DataField="cus_name" HeaderText="姓名" />
                    <asp:BoundField DataField="train_id" HeaderText="火車編號" />
                    <asp:BoundField DataField="seat_id" HeaderText="座位編號" />
                    <asp:BoundField DataField="o_stn" HeaderText="起站點" />
                    <asp:BoundField DataField="d_stn" HeaderText="到站點" />
                    <%--<asp:CommandField ShowDeleteButton="true" DeleteText="取消訂票"/>--%>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:Button ID="del" runat="server" Text="取消訂票" OnClientClick="return confirm('確定取消訂票?');" CommandName="del" CommandArgument='<%#Eval("bookid") %>' />
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="show" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>

