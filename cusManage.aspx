﻿<%@ Page Title="顧客管理" Language="C#" MasterPageFile="~/adminMasterPage.master" AutoEventWireup="true" CodeFile="cusManage.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        table {
            margin-left: auto;
            margin-right: auto;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:trainConnectionString %>"
        DeleteCommand="del_book"
        DeleteCommandType="StoredProcedure">
        <DeleteParameters>
            <asp:Parameter Name="book_id" Type="Int64" />
        </DeleteParameters>
    </asp:SqlDataSource>
    <div class="row text-center">
        <div class="col-xs-12 text-center">
            <div class="row" style="height:50px;"></div>
            <asp:ListView ID="ListView1" runat="server" DataSourceID="SqlDataSource1" OnItemCommand="ListView1_ItemCommand">
                <LayoutTemplate>
                    <table border="1" style="width:80%;">
                        <tr>
                            <td>訂票編號
                            </td>
                            <td>乘客姓名
                            </td>
                            <td>火車編號
                            </td>
                            <td>座位編號
                            </td>
                            <td>起站點
                            </td>
                            <td>到站點
                            </td>
                            <td></td>
                        </tr>
                        <asp:PlaceHolder ID="itemPlaceHolder" runat="server"></asp:PlaceHolder>
                    </table>
                </LayoutTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%#Eval("bookid") %></td>
                        <td><%#Eval("cus_name") %></td>
                        <td><%#Eval("train_id") %></td>
                        <td><%#Eval("seat_id") %></td>
                        <td><%#Eval("o_stn") %></td>
                        <td><%#Eval("d_stn") %></td>
                        <td>
                            <asp:Button ID="del_btn" runat="server" Text="刪除" OnClientClick="return confirm('確定刪除?');" CommandName="del" CommandArgument='<%#Eval("bookid") %>' />
                        </td>
                    </tr>
                </ItemTemplate>
            </asp:ListView>
        </div>
    </div>
</asp:Content>

