﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["check"] = "abcd1234";
        if (!IsPostBack)
        {
            if (Session["check"] == null)
            {
                Response.Redirect("admin.aspx");
            }
            else
            {
                if (Session["check"].ToString() != "abcd1234")
                {
                    Response.Redirect("admin.aspx");
                }
            }
            new_ddl_time();
        }
        string select_string = "select Train.train_id,a.stn_name as start_stn,b.stn_name as end_stn,Train.date_time"
                                + " from Train"
                                + " inner join Station as a on Train.start_stn = a.stn_id"
                                + " inner join Station as b on Train.end_stn = b.stn_id"
                                + " order by Train.train_id";
        SqlDataSource1.SelectCommand = select_string;
    }




    protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "detail")
        {
            var lv = (ListView)e.Item.FindControl("ListView2");
            lv.DataSourceID = "SqlDataSource2";
        }
        else if (e.CommandName == "del")
        {
            SqlDataSource1.DeleteParameters["train_id"].DefaultValue = e.CommandArgument.ToString();
            SqlDataSource1.Delete();
        }
    }
    public void new_ddl_time()
    {
        int year, mouth, day, hour, minute, second;
        year = 2116;
        mouth = 12;
        day = 31;
        hour = 23;
        minute = 59;
        second = 59;
        for (int i = 2016; i <= year; i += 1)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            ddl_y.Items.Add(li);
        }
        for (int i = 1; i <= mouth; i += 1)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            ddl_m.Items.Add(li);
        }
        for (int i = 1; i <= day; i += 1)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            ddl_d.Items.Add(li);
        }
        for (int i = 0; i <= hour; i += 1)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            ddl_hh.Items.Add(li);
        }
        for (int i = 0; i <= minute; i += 1)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            ddl_mm.Items.Add(li);
        }
        for (int i = 0; i <= second; i += 1)
        {
            ListItem li = new ListItem(i.ToString(), i.ToString());
            ddl_ss.Items.Add(li);
        }
    }

    protected void btn_new_Click(object sender, EventArgs e)
    {
        string date = "";
        date = string.Format("{0}-{1}-{2} {3}:{4}:{5}.000", ddl_y.SelectedValue.ToString(), ddl_m.SelectedValue.ToString(), ddl_d.SelectedValue.ToString(),
                                                    ddl_hh.SelectedValue.ToString(), ddl_mm.SelectedValue.ToString(), ddl_ss.SelectedValue.ToString());
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["trainConnectionString"].ConnectionString);
        conn.Open();
        SqlCommand cmd = new SqlCommand("new_train", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@start", SqlDbType.Int).Value = Convert.ToInt32(ddl_start.SelectedValue);
        cmd.Parameters.Add("@end", SqlDbType.Int).Value = Convert.ToInt32(ddl_end.SelectedValue);
        cmd.Parameters.Add("@date", SqlDbType.DateTime).Value = Convert.ToDateTime(date);
        cmd.Parameters.Add("@max", SqlDbType.Int).Value = Convert.ToInt32(ddl_max_people.SelectedValue);
        cmd.ExecuteNonQuery();
        SqlDataSource1.DataBind();
        ListView1.DataBind();
    }
}