﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="admin.aspx.cs" Inherits="admin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>管理員登入</title>
    <link href="Content/bootstrap.min.css" rel="stylesheet" />
    <link href="Content/Custom.css" rel="stylesheet" />
    <script src="Scripts/jquery-2.2.3.min.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>
<body>
    
    <form id="form1" runat="server">
        <div style="background-color: aqua; height: 1000px;">
            <div class="row" style="height: 100px;"></div>
            <div class="col-xs-4 col-xs-offset-4" style="height: 300px; background-color: white;">
                <div class="row">
                    <h1>火車管理系統</h1>
                </div>
                <div class="col-xs-12 text-center">
                    <table>
                        <tr>
                            <td>帳號<asp:TextBox ID="txt_user" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>密碼<asp:TextBox ID="txt_pwd" runat="server" TextMode="Password"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp</td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Button ID="btn_go" runat="server" Text="登入" OnClick="btn_go_Click" />
                            </td>
                        </tr>
                    </table>
                    <asp:Label ID="show_msg" runat="server" Text="" ForeColor="Red"></asp:Label>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
