﻿<%@ Page Title="查詢" Language="C#" MasterPageFile="~/customerMasterPage.master" AutoEventWireup="true" CodeFile="search.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .calendar {
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    


    <div class="row" style="height: 30px;">
    </div>
    <div class="row">
        <div class="col-xs-12 text-center">
            日期：<asp:Label ID="Label1" runat="server" Text="Label" BackColor="White"></asp:Label>
            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/src/more.png" OnClick="ImageButton1_Click" />
            起程站：<asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Value="1">南港</asp:ListItem>
                <asp:ListItem Value="2">台北</asp:ListItem>
                <asp:ListItem Value="3">板橋</asp:ListItem>
                <asp:ListItem Value="4">桃園</asp:ListItem>
                <asp:ListItem Value="5">新竹</asp:ListItem>
                <asp:ListItem Value="6">苗栗</asp:ListItem>
                <asp:ListItem Value="7">台中</asp:ListItem>
                <asp:ListItem Value="8">彰化</asp:ListItem>
                <asp:ListItem Value="9">雲林</asp:ListItem>
                <asp:ListItem Value="10">台南</asp:ListItem>
                <asp:ListItem Value="11">左營</asp:ListItem>
            </asp:DropDownList>
            到達站：<asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem Value="1">南港</asp:ListItem>
                <asp:ListItem Value="2">台北</asp:ListItem>
                <asp:ListItem Value="3">板橋</asp:ListItem>
                <asp:ListItem Value="4">桃園</asp:ListItem>
                <asp:ListItem Value="5">新竹</asp:ListItem>
                <asp:ListItem Value="6">苗栗</asp:ListItem>
                <asp:ListItem Value="7">台中</asp:ListItem>
                <asp:ListItem Value="8">彰化</asp:ListItem>
                <asp:ListItem Value="9">雲林</asp:ListItem>
                <asp:ListItem Value="10">台南</asp:ListItem>
                <asp:ListItem Value="11">左營</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="Button1" runat="server" Text="查詢" OnClick="Button1_Click" />
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <asp:Panel ID="pl1" runat="server" Visible="false">
                <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged"></asp:Calendar>
            </asp:Panel>
        </div>
    </div>
    <div class="row" style="height:50px;"></div>
    <div class="row text-center">
        <div class="col-xs-12" >
            <asp:Label ID="data_table" runat="server" Text=""></asp:Label>
        </div>
    </div>
</asp:Content>

