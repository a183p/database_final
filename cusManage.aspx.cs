﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["check"] = "abcd1234";
        if (!IsPostBack)
        {
            if (Session["check"] == null)
            {
                Response.Redirect("admin.aspx");
            }
            else
            {
                if (Session["check"].ToString() != "abcd1234")
                {
                    Response.Redirect("admin.aspx");
                }
            }
        }
        string search_book = "select Customer.bookid,Customer.cus_name,Seat.train_id,Seat.seat_id,"
                               + "a.stn_name as o_stn,b.stn_name as d_stn from Customer"
                                + " inner join Seat on Customer.bookid = Seat.book_id"
                                + " inner join Station as a on seat.origin_stn = a.stn_id"
                                + " inner join Station as b on seat.dest_stn = b.stn_id"
                                + " order by Customer.bookid";
        SqlDataSource1.SelectCommand = search_book;
    }

    protected void ListView1_ItemCommand(object sender, ListViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            SqlDataSource1.DeleteParameters["book_id"].DefaultValue = e.CommandArgument.ToString();
            SqlDataSource1.Delete();
        }
    }
}