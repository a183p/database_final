﻿  <%@ Page Title="火車訂票系統" Language="C#" MasterPageFile="~/customerMasterPage.master" AutoEventWireup="true" CodeFile="index.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style>
        .imgtest {
            height: 100px;
            width: 100px;
            opacity: 1;
            transition: 0.3s ease;
            cursor: pointer;
            position: absolute;
            left: 50%;
            margin-left: -50px;
        }

            .imgtest:hover {
                transform: scale(1.5, 1.5);
                opacity: 0.7;
            }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div>
        <div class="row" style="height: 150px;">
        </div>
        <div class="row" style="height: 150px;">
            <div class="col-xs-4" style="height: 150px;">
                <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/src/ticket2.jpg" CssClass="imgtest" PostBackUrl="~/book.aspx" />
            </div>
            <div class="col-xs-4" style="height: 150px;">
                <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/src/search.png" CssClass="imgtest" PostBackUrl="~/search.aspx" />
            </div>
            <div class="col-xs-4" style="height: 150px;">
                <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/src/document.png" CssClass="imgtest" PostBackUrl="~/book_result_search.aspx" />
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-4 text-center" style="height: 75px;">訂票</div>
        <div class="col-xs-4 text-center" style="height: 75px;">查詢班次</div>
        <div class="col-xs-4 text-center" style="height: 75px;">查詢訂票資訊</div>
    </div>

</asp:Content>

