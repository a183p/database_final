﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Calendar1.SelectedDate = DateTime.Today;
            Label1.Text = DateTime.Today.ToString("yyyy/MM/dd");
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string creat_func = "";
        int count = 0;
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["trainConnectionString"].ConnectionString);
        conn.Open();
        SqlCommand cmd = new SqlCommand("train_search", conn);
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.Parameters.Add("@up", SqlDbType.Int).Value = Convert.ToInt32(DropDownList1.SelectedValue);
        cmd.Parameters.Add("@down", SqlDbType.Int).Value = Convert.ToInt32(DropDownList2.SelectedValue);
        cmd.Parameters.Add("@set_time1", SqlDbType.DateTime2).Value =
            Convert.ToDateTime(Label1.Text.ToString() + " 00:00:00.000");
        cmd.Parameters.Add("@set_time2", SqlDbType.DateTime2).Value =
            Convert.ToDateTime(Label1.Text.ToString() + " 23:59:59.000");
        SqlDataReader read = cmd.ExecuteReader();
        creat_func += "$(document).ready(function() {";
        data_table.Text = "";
        data_table.Text += "<table border='1'>";
        data_table.Text += "<tr>";
        data_table.Text += "<td>" + "火車編號" + "<td>";
        data_table.Text += "<td>" + "發車站" + "<td>";
        data_table.Text += "<td>" + "終點站" + "<td>";
        data_table.Text +=
            "<td>" + DropDownList1.SelectedItem.Text + "開車時間" + "<td>";
        data_table.Text +=
            "<td>" + DropDownList2.SelectedItem.Text + "抵達時間" + "<td>";
        data_table.Text += "<td>" + "剩餘人數" + "<td>";
        data_table.Text += "<td>" + "訂票" + "<td>";
        data_table.Text += "</tr>";

        while (read.Read())
        {
            if (DropDownList1.SelectedValue == DropDownList2.SelectedValue)
                break;
            if (read["train_id"].ToString() != "")
            {
                int remain = remain_people(Convert.ToInt32(read[0]), Convert.ToInt32(read[6]), Convert.ToInt32(DropDownList1.SelectedValue), Convert.ToInt32(DropDownList2.SelectedValue));
                data_table.Text += "<tr>";
                data_table.Text += "<td>" + read[0].ToString() + "<td>";
                data_table.Text += "<td>" + read[1].ToString() + "<td>";
                data_table.Text += "<td>" + read[2].ToString() + "<td>";
                data_table.Text += "<td>" + calc_time(Convert.ToDateTime(read[3]), Convert.ToInt32(read[4]) - Convert.ToInt32(DropDownList1.SelectedValue)) + "<td>";
                data_table.Text += "<td>" + calc_time(Convert.ToDateTime(read[3]), Convert.ToInt32(read[4]) - Convert.ToInt32(DropDownList2.SelectedValue)) + "<td>";
                data_table.Text += "<td>" + remain.ToString() + "<td>";
                data_table.Text += "<td>" + "<input type='button' id='btn" + count.ToString() + "' value='訂票' />" + "</td>";
                creat_func += btn_func_create(count, Convert.ToInt32(read[0]));
                data_table.Text += "</tr>";
                count += 1;
            }
        }
        data_table.Text += "</table>";
        creat_func += "});";
        if (count == 0)
            data_table.Text += "<br />" + "查無資料";
        cmd.Cancel();
        read.Close();
        conn.Close();
        ClientScript.RegisterClientScriptBlock(this.GetType(), "ConfirmSubmit", creat_func, true);
    }

    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {
        if (!pl1.Visible)
            pl1.Visible = true;
        else
            pl1.Visible = false;
    }
    protected void Calendar1_SelectionChanged(object sender, EventArgs e)
    {
        Label1.Text = Calendar1.SelectedDate.ToString("yyyy/MM/dd");
    }
    public string calc_time(System.DateTime time, int n)
    {
        n = Math.Abs(n);
        time = time.AddMinutes(30 * n);
        return time.ToString();
    }
    public string btn_func_create(int n, int id)
    {
        return "$('#btn" + n + "').click(function () {window.location.href = './book.aspx?train_id=" + id + "&start=" + DropDownList1.SelectedValue + "&end=" + DropDownList2.SelectedValue + "+&date=" + Calendar1.SelectedDate.ToString("yyyy/MM/dd") + "';});";
    }
    public int remain_people(int train_id, int max, int start, int end)
    {
        SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["trainConnectionString"].ConnectionString);
        conn.Open();
        SqlCommand cmd = new SqlCommand();
        cmd.Connection = conn;
        if (start < end)
            cmd.CommandText = "select count(*) from Seat where train_id=" + train_id.ToString() + " and ((dest_stn> " + start.ToString() + " and dest_stn <=" + end.ToString() + ") or (origin_stn<" + end.ToString() + " and origin_stn >=" + end.ToString() + "))";
        else
            cmd.CommandText = "select count(*) from Seat where train_id=" + train_id.ToString() + " and ((dest_stn< " + start.ToString() + "and dest_stn >=" + end.ToString() + ") or (origin_stn <= " + start.ToString() + " and origin_stn <" + end.ToString() + "))";
        int remain = max - (int)cmd.ExecuteScalar();
        return remain;
    }
}
